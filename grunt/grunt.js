/* build the javascript "grunt lint min" */
/* build the css "grunt less mincss concat" */
/* build it all "grunt lint jst min less mincss concat" */

module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-mincss');
  grunt.loadNpmTasks('grunt-contrib-jst');
  grunt.loadNpmTasks('grunt-shell');

  /* if you simply run "grunt" these default tasks will execute, in order */
  grunt.registerTask('default', "lint jst min less mincss shell");

  grunt.initConfig({

    /* banner goes into the min files */
    meta: {
      banner: '/* generated: <%= grunt.template.today("yyyy-mm-dd @ hh:MM:ss") %> */'
    },

    // ##################### JAVASCRIPT ############################ //

    /* lint your own source files, not all of them, see jshint below for options  */
    lint: {
      files: ['../js/max/pinnable.js']
    },

    jst: {
      compile: {
        options: {
          namespace: 'pinnable'
        },
        files: {
          "../js/templates.js": ["../jst/**/*.tmpl"]
        }
      }
    },

    /* only minify what needs to be minified */
    /* this could be written programatically since it's javascript, not simple json */
    min: {
      pinnable: {
        src: ['<banner>', '../js/max/pinnable.js', '../js/templates.js'],
        dest: '../js/pinnable.min.js'
      },
    },

    /* use of the less directive requires grunt-contrib */
    less: {
      compile: {
        files: {
          '../css/max/pinnable.css' : '../less/style.less'
        },
        options: {
          paths: ['../less']
        }
      }
    },

    /* mincss is primitive at the moment (dest : src) */
    mincss: {
        '../css/pinnable.css': '../css/max/pinnable.css'
    },

    /* http://www.jshint.com/options/ */
    /* NOTE: lint rules in the spurce files will override these settings! */
    jshint: {
      options: {
        indent: 2, /* indent sets white to true in jshint 0.9.1, but we want it to be false */
        white: false, /* so white goes after indent gets set */
        curly: true, /* requires you to always put curly braces around blocks in loops and conditionals */
        devel: true, /* expose console.log and alert and other globals */
        eqeqeq: true, /* prohibits the use of == and != in favor of === and !== */
        immed: true, /* prohibits the use of immediate function invocations without wrapping them in parentheses */
        latedef: true, /* prohibits the use of a variable before it was defined (ie, no hoisting) */
        newcap: true, /* requires you to capitalize names of constructor functions */
        noarg: true, /* prohibits the use of arguments.caller and arguments.callee (ecma5 strict) */
        sub: true, /* suppresses warnings about using [] notation when it can be expressed in dot notation */
        strict: true, /* requires all functions to run in EcmaScript 5's strict mode */
        undef: true, /* no explicitly undefined vars (ie, no spontaneous globals) */
        boss: true, /* suppresses warnings about the use of assignments in cases where comparisons are expected */
        eqnull: true, /* suppresses warnings about == null comparisons */
        browser: true, /* defines globals exposed by modern browsers */
        laxcomma: true /* suppresses warnings about comma-first coding style */
      },
      globals: {
        jQuery: true,
        moment: true,
        Modernizr: true,
        self: true,
        _: true
      }
    },

    shell: {
        copy: {
            command: "scp -r ../{js,css,tests,images} pkillian.me:~/www/pinnable",
            stdout: true,
            stderr: true,
            failOnError: true
        }
    }
  });
};
    
