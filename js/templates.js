this["pinnable"] = this["pinnable"] || {};

this["pinnable"]["../jst/pin.tmpl"] = function(obj){
var __p='';var print=function(){__p+=Array.prototype.join.call(arguments, '')};
with(obj||{}){
__p+='<div class="pin_div" id="pin_'+
( num )+
'" style="display: none;">\n    <img class="pin_img" id="img_'+
( num )+
'" src="'+
( image )+
'"/>\n    ';
 if (caption) { 
;__p+='\n        <span class="pin_cap">'+
( caption )+
'</span>\n    ';
 } 
;__p+='\n</div>';
}
return __p;
};