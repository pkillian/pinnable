var pinnable = pinnable || {};

(function(that, $, document, undefined) {

    "use strict"; /* use ecma5 strict mode inside the scope of the object */

    this.initPinBoard = function(pinBoard) {

        pinBoard.pinnable = that;                                                   // Pointer to 'pinnable' namespace
        pinBoard.fade_length = pinBoard.fade_length || 800;                         // Transition speed when rendering
        pinBoard.fade_start_delay = pinBoard.fade_start_delay || 300;               // MS to wait before animations start
        pinBoard.container = pinBoard.container || "#pinboard";                     // Container to populate
        pinBoard.num_images = pinBoard.num_images || 16;                            // Total number of images
        pinBoard.num_wide = pinBoard.num_wide || 4;                                 // How many images wide         TODO
        pinBoard.media_loc = pinBoard.media_loc || "../images";                     // Root folder for media (pins, captions)
        pinBoard.image_loc = pinBoard.image_loc || pinBoard.media_loc + "/pins";
        pinBoard.caption_loc = pinBoard.caption_loc || pinBoard.media_loc + "/captions";
        pinBoard.image_list = pinBoard.image_list || [];                            // List of image names          TODO
        pinBoard.image_ext = pinBoard.image_ext || ".jpg";                          // Image file type
        pinBoard.tmpl = pinBoard.tmpl || "../jst/pin.tmpl";
        pinBoard.render_pin = function(pin_num) {
            if (pin_num > this.num_images) {
                return;
            }

            var render = this;                              // Function to render the whole board
            var pTmpl = render.pinnable[render.tmpl];
            var pCaption;

            $.ajax({
                url: render.caption_loc + "/caption_" + pin_num + ".txt",
                dataType: "text",
                async: false
            })
            .done(function (obj) {
                $(pinBoard.container).append(
                    pTmpl({
                        num: pin_num,
                        image: render.image_loc + "/pin_" + pin_num + render.image_ext,
                        caption: obj
                    })
                );
            })
            .fail(function(data, textStatus, jqXHR) {
                console.log("Failed to get '" + render.caption_loc + "/caption_" + pin_num + ".txt'!");
                $(pinBoard.container).append(
                    pTmpl({
                        num: pin_num,
                        image: render.image_loc + "/pin_" + pin_num + render.image_ext,
                        caption: false
                    })
                );
            });
        };
        pinBoard.render = function() {
            var render = this;

            var nullFunc = function() {};

            for (var i = 1; i <= this.num_images; i++) {
                this.render_pin(i);
            }

            $(this.container).children().each(function(index) {
                $(this).delay(render.fade_start_delay * index).fadeIn(render.fade_length);
            });
        };

        return pinBoard;
    };

}.call(pinnable, pinnable, window.jQuery, document));
